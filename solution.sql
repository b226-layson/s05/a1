=======================================
	MYSQL- CULMINATING ACTIVITY
=======================================

/* USE classic_models;

DESCRIBE customers;

SELECT * FROM customers; */

/* 
MariaDB [classic_models]> DESCRIBE customers;
+------------------------+---------------+------+-----+---------+-------+
| Field                  | Type          | Null | Key | Default | Extra |
+------------------------+---------------+------+-----+---------+-------+
| customerNumber         | int(11)       | NO   | PRI | NULL    |       |
| customerName           | varchar(50)   | NO   |     | NULL    |       |
| contactLastName        | varchar(50)   | NO   |     | NULL    |       |
| contactFirstName       | varchar(50)   | NO   |     | NULL    |       |
| phone                  | varchar(50)   | NO   |     | NULL    |       |
| addressLine1           | varchar(50)   | NO   |     | NULL    |       |
| addressLine2           | varchar(50)   | YES  |     | NULL    |       |
| city                   | varchar(50)   | NO   |     | NULL    |       |
| state                  | varchar(50)   | YES  |     | NULL    |       |
| postalCode             | varchar(15)   | YES  |     | NULL    |       |
| country                | varchar(50)   | NO   |     | NULL    |       |
| salesRepEmployeeNumber | int(11)       | YES  | MUL | NULL    |       |
| creditLimit            | decimal(10,2) | YES  |     | NULL    |       |
+------------------------+---------------+------+-----+---------+-------+
13 rows in set (0.016 sec)
 */

-- 1. Return the customerName of the customers who are from the Philippines

SELECT customerName FROM customers WHERE country = "Philippines";

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. Return the product name and MSRP of the product named "The Titanic"

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

/* 
MariaDB [classic_models]> SHOW TABLES;
+--------------------------+
| Tables_in_classic_models |
+--------------------------+
| customers                |
| employees                |
| offices                  |
| orderdetails             |
| orders                   |
| payments                 |
| productlines             |
| products                 |
+--------------------------+
8 rows in set (0.002 sec)
 */

/* 
DESCRIBE products;

MariaDB [classic_models]> DESCRIBE products;
+--------------------+---------------+------+-----+---------+-------+
| Field              | Type          | Null | Key | Default | Extra |
+--------------------+---------------+------+-----+---------+-------+
| productCode        | varchar(15)   | NO   | PRI | NULL    |       |
| productName        | varchar(70)   | NO   |     | NULL    |       |
| productLine        | varchar(50)   | NO   | MUL | NULL    |       |
| productScale       | varchar(10)   | NO   |     | NULL    |       |
| productVendor      | varchar(50)   | NO   |     | NULL    |       |
| productDescription | text          | NO   |     | NULL    |       |
| quantityInStock    | smallint(6)   | NO   |     | NULL    |       |
| buyPrice           | decimal(10,2) | NO   |     | NULL    |       |
| MSRP               | decimal(10,2) | NO   |     | NULL    |       |
+--------------------+---------------+------+-----+---------+-------+
9 rows in set (0.013 sec)
 */


-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"

SELECT lastName, firstName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

/* 
DESCRIBE employees;

MariaDB [classic_models]> DESCRIBE employees;
+----------------+--------------+------+-----+---------+-------+
| Field          | Type         | Null | Key | Default | Extra |
+----------------+--------------+------+-----+---------+-------+
| employeeNumber | int(11)      | NO   | PRI | NULL    |       |
| lastName       | varchar(50)  | NO   |     | NULL    |       |
| firstName      | varchar(50)  | NO   |     | NULL    |       |
| extension      | varchar(10)  | NO   |     | NULL    |       |
| email          | varchar(100) | NO   |     | NULL    |       |
| officeCode     | varchar(10)  | NO   | MUL | NULL    |       |
| reportsTo      | int(11)      | YES  | MUL | NULL    |       |
| jobTitle       | varchar(50)  | NO   |     | NULL    |       |
+----------------+--------------+------+-----+---------+-------+
8 rows in set (0.015 sec)
 */

-- 5. Return the names of customers who have no registered state
SELECT customerName FROM customers WHERE state IS NULL;
/* 
DESCRIBE customers;

MariaDB [classic_models]> DESCRIBE customers;
+------------------------+---------------+------+-----+---------+-------+
| Field                  | Type          | Null | Key | Default | Extra |
+------------------------+---------------+------+-----+---------+-------+
| customerNumber         | int(11)       | NO   | PRI | NULL    |       |
| customerName           | varchar(50)   | NO   |     | NULL    |       |
| contactLastName        | varchar(50)   | NO   |     | NULL    |       |
| contactFirstName       | varchar(50)   | NO   |     | NULL    |       |
| phone                  | varchar(50)   | NO   |     | NULL    |       |
| addressLine1           | varchar(50)   | NO   |     | NULL    |       |
| addressLine2           | varchar(50)   | YES  |     | NULL    |       |
| city                   | varchar(50)   | NO   |     | NULL    |       |
| state                  | varchar(50)   | YES  |     | NULL    |       |
| postalCode             | varchar(15)   | YES  |     | NULL    |       |
| country                | varchar(50)   | NO   |     | NULL    |       |
| salesRepEmployeeNumber | int(11)       | YES  | MUL | NULL    |       |
| creditLimit            | decimal(10,2) | YES  |     | NULL    |       |
+------------------------+---------------+------+-----+---------+-------+
13 rows in set (0.017 sec)
 */

-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve

SELECT lastName, firstName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

/* 
DESCRIBE employees;

MariaDB [classic_models]> DESCRIBE employees;
+----------------+--------------+------+-----+---------+-------+
| Field          | Type         | Null | Key | Default | Extra |
+----------------+--------------+------+-----+---------+-------+
| employeeNumber | int(11)      | NO   | PRI | NULL    |       |
| lastName       | varchar(50)  | NO   |     | NULL    |       |
| firstName      | varchar(50)  | NO   |     | NULL    |       |
| extension      | varchar(10)  | NO   |     | NULL    |       |
| email          | varchar(100) | NO   |     | NULL    |       |
| officeCode     | varchar(10)  | NO   | MUL | NULL    |       |
| reportsTo      | int(11)      | YES  | MUL | NULL    |       |
| jobTitle       | varchar(50)  | NO   |     | NULL    |       |
+----------------+--------------+------+-----+---------+-------+
8 rows in set (0.015 sec)
 */

-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;


/* 
SELECT country FROM customers;
DESCRIBE customers;

MariaDB [classic_models]> DESCRIBE customers;
+------------------------+---------------+------+-----+---------+-------+
| Field                  | Type          | Null | Key | Default | Extra |
+------------------------+---------------+------+-----+---------+-------+
| customerNumber         | int(11)       | NO   | PRI | NULL    |       |
| customerName           | varchar(50)   | NO   |     | NULL    |       |
| contactLastName        | varchar(50)   | NO   |     | NULL    |       |
| contactFirstName       | varchar(50)   | NO   |     | NULL    |       |
| phone                  | varchar(50)   | NO   |     | NULL    |       |
| addressLine1           | varchar(50)   | NO   |     | NULL    |       |
| addressLine2           | varchar(50)   | YES  |     | NULL    |       |
| city                   | varchar(50)   | NO   |     | NULL    |       |
| state                  | varchar(50)   | YES  |     | NULL    |       |
| postalCode             | varchar(15)   | YES  |     | NULL    |       |
| country                | varchar(50)   | NO   |     | NULL    |       |
| salesRepEmployeeNumber | int(11)       | YES  | MUL | NULL    |       |
| creditLimit            | decimal(10,2) | YES  |     | NULL    |       |
+------------------------+-
 */

-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

/* 
SELECT comments FROM orders;
DESCRIBE orders;


MariaDB [classic_models]> DESCRIBE orders;
+----------------+-------------+------+-----+---------+-------+
| Field          | Type        | Null | Key | Default | Extra |
+----------------+-------------+------+-----+---------+-------+
| orderNumber    | int(11)     | NO   | PRI | NULL    |       |
| orderDate      | date        | NO   |     | NULL    |       |
| requiredDate   | date        | NO   |     | NULL    |       |
| shippedDate    | date        | YES  |     | NULL    |       |
| status         | varchar(15) | NO   |     | NULL    |       |
| comments       | text        | YES  |     | NULL    |       |
| customerNumber | int(11)     | NO   | MUL | NULL    |       |
+----------------+-------------+------+-----+---------+-------+
7 rows in set (0.015 sec)
 */
-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productline FROM productlines WHERE textDescription LIKE '%state of the art%';
/* 
DESCRIBE productlines;
SELECT productDescription FROM products;
DESCRIBE products;
SELECT * FROM products WHERE productLine = "Motorcycles";

SELECT productDescription FROM products WHERE productLine = "Motorcycles";

MariaDB [classic_models]> DESCRIBE products;
+--------------------+---------------+------+-----+---------+-------+
| Field              | Type          | Null | Key | Default | Extra |
+--------------------+---------------+------+-----+---------+-------+
| productCode        | varchar(15)   | NO   | PRI | NULL    |       |
| productName        | varchar(70)   | NO   |     | NULL    |       |
| productLine        | varchar(50)   | NO   | MUL | NULL    |       |
| productScale       | varchar(10)   | NO   |     | NULL    |       |
| productVendor      | varchar(50)   | NO   |     | NULL    |       |
| productDescription | text          | NO   |     | NULL    |       |
| quantityInStock    | smallint(6)   | NO   |     | NULL    |       |
| buyPrice           | decimal(10,2) | NO   |     | NULL    |       |
| MSRP               | decimal(10,2) | NO   |     | NULL    |       |
+--------------------+---------------+------+-----+---------+-------+
9 rows in set (0.014 sec)

 */

-- 10. Return the countries of customers without duplication
SELECT DISTINCT country FROM customers;

/* 
DESCRIBE customers;

MariaDB [classic_models]> DESCRIBE customers;
+------------------------+---------------+------+-----+---------+-------+
| Field                  | Type          | Null | Key | Default | Extra |
+------------------------+---------------+------+-----+---------+-------+
| customerNumber         | int(11)       | NO   | PRI | NULL    |       |
| customerName           | varchar(50)   | NO   |     | NULL    |       |
| contactLastName        | varchar(50)   | NO   |     | NULL    |       |
| contactFirstName       | varchar(50)   | NO   |     | NULL    |       |
| phone                  | varchar(50)   | NO   |     | NULL    |       |
| addressLine1           | varchar(50)   | NO   |     | NULL    |       |
| addressLine2           | varchar(50)   | YES  |     | NULL    |       |
| city                   | varchar(50)   | NO   |     | NULL    |       |
| state                  | varchar(50)   | YES  |     | NULL    |       |
| postalCode             | varchar(15)   | YES  |     | NULL    |       |
| country                | varchar(50)   | NO   |     | NULL    |       |
| salesRepEmployeeNumber | int(11)       | YES  | MUL | NULL    |       |
| creditLimit            | decimal(10,2) | YES  |     | NULL    |       |
+------------------------+---------------+------+-----+---------+-------+
13 rows in set (0.011 sec)
 */

-- 11. Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;

/* 
DESCRIBE orders;


MariaDB [classic_models]> DESCRIBE orders;
+----------------+-------------+------+-----+---------+-------+
| Field          | Type        | Null | Key | Default | Extra |
+----------------+-------------+------+-----+---------+-------+
| orderNumber    | int(11)     | NO   | PRI | NULL    |       |
| orderDate      | date        | NO   |     | NULL    |       |
| requiredDate   | date        | NO   |     | NULL    |       |
| shippedDate    | date        | YES  |     | NULL    |       |
| status         | varchar(15) | NO   |     | NULL    |       |
| comments       | text        | YES  |     | NULL    |       |
| customerNumber | int(11)     | NO   | MUL | NULL    |       |
+----------------+-------------+------+-----+---------+-------+
7 rows in set (0.021 sec)
 */


-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada

SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

/* 
SHOW TABLES;
DESCRIBE employees;

MariaDB [classic_models]> DESCRIBE customers;
+------------------------+---------------+------+-----+---------+-------+
| Field                  | Type          | Null | Key | Default | Extra |
+------------------------+---------------+------+-----+---------+-------+
| customerNumber         | int(11)       | NO   | PRI | NULL    |       |
| customerName           | varchar(50)   | NO   |     | NULL    |       |
| contactLastName        | varchar(50)   | NO   |     | NULL    |       |
| contactFirstName       | varchar(50)   | NO   |     | NULL    |       |
| phone                  | varchar(50)   | NO   |     | NULL    |       |
| addressLine1           | varchar(50)   | NO   |     | NULL    |       |
| addressLine2           | varchar(50)   | YES  |     | NULL    |       |
| city                   | varchar(50)   | NO   |     | NULL    |       |
| state                  | varchar(50)   | YES  |     | NULL    |       |
| postalCode             | varchar(15)   | YES  |     | NULL    |       |
| country                | varchar(50)   | NO   |     | NULL    |       |
| salesRepEmployeeNumber | int(11)       | YES  | MUL | NULL    |       |
| creditLimit            | decimal(10,2) | YES  |     | NULL    |       |
+------------------------+---------------+------+-----+---------+-------+
13 rows in set (0.026 sec)
 */


-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT employees.firstName, employees.lastName, offices.city FROM employees
    RIGHT JOIN offices ON employees.officeCode = offices.officeCode
	WHERE city = "Tokyo";

/* 
DESCRIBE offices;

SELECT * FROM offices;

SELECT * FROM employees;

MariaDB [classic_models]> DESCRIBE offices;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| officeCode   | varchar(10) | NO   | PRI | NULL    |       |
| city         | varchar(50) | NO   |     | NULL    |       |
| phone        | varchar(50) | NO   |     | NULL    |       |
| addressLine1 | varchar(50) | NO   |     | NULL    |       |
| addressLine2 | varchar(50) | YES  |     | NULL    |       |
| state        | varchar(50) | YES  |     | NULL    |       |
| country      | varchar(50) | NO   |     | NULL    |       |
| postalCode   | varchar(15) | NO   |     | NULL    |       |
| territory    | varchar(10) | NO   |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+
9 rows in set (0.011 sec)
 */
-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"

SELECT customers.customerName FROM customers
    RIGHT JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE lastName = "Thompson";

/* 
SELECT salesRepEmployeeNumber FROM customers;

SELECT * FROM employees WHERE lastName = "Thompson";

DESCRIBE customers;

MariaDB [classic_models]> DESCRIBE customers;
+------------------------+---------------+------+-----+---------+-------+
| Field                  | Type          | Null | Key | Default | Extra |
+------------------------+---------------+------+-----+---------+-------+
| customerNumber         | int(11)       | NO   | PRI | NULL    |       |
| customerName           | varchar(50)   | NO   |     | NULL    |       |
| contactLastName        | varchar(50)   | NO   |     | NULL    |       |
| contactFirstName       | varchar(50)   | NO   |     | NULL    |       |
| phone                  | varchar(50)   | NO   |     | NULL    |       |
| addressLine1           | varchar(50)   | NO   |     | NULL    |       |
| addressLine2           | varchar(50)   | YES  |     | NULL    |       |
| city                   | varchar(50)   | NO   |     | NULL    |       |
| state                  | varchar(50)   | YES  |     | NULL    |       |
| postalCode             | varchar(15)   | YES  |     | NULL    |       |
| country                | varchar(50)   | NO   |     | NULL    |       |
| salesRepEmployeeNumber | int(11)       | YES  | MUL | NULL    |       |
| creditLimit            | decimal(10,2) | YES  |     | NULL    |       |
+------------------------+---------------+------+-----+---------+-------+
13 rows in set (0.019 sec)

DESCRIBE employees;

 */

-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"


SELECT products.productName, customers.customerName FROM customers
		JOIN orders ON customers.customerNumber = orders.customerNumber
		JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
		JOIN products ON orderdetails.productCode = products.productCode
		WHERE customerName = "Baane Mini Imports";

/* 


DESCRIBE customers;

DESCRIBE products;

SELECT productScale FROM products;

SELECT customerName FROM customers WHERE customerName = "Baane Mini Imports";

 */


-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country



SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM offices
       	JOIN employees ON offices.officeCode = employees.officeCode
    	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
		WHERE offices.country = customers.country;



/* 
DESCRIBE employees;
DESCRIBE customers;
DESCRIBE offices;

 */

-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000

SELECT products.productName, products.quantityInStock FROM products WHERE productLine = "planes" && quantityInStock < 1000;

/* 
DESCRIBE products;

MariaDB [classic_models]> DESCRIBE products;
+--------------------+---------------+------+-----+---------+-------+
| Field              | Type          | Null | Key | Default | Extra |
+--------------------+---------------+------+-----+---------+-------+
| productCode        | varchar(15)   | NO   | PRI | NULL    |       |
| productName        | varchar(70)   | NO   |     | NULL    |       |
| productLine        | varchar(50)   | NO   | MUL | NULL    |       |
| productScale       | varchar(10)   | NO   |     | NULL    |       |
| productVendor      | varchar(50)   | NO   |     | NULL    |       |
| productDescription | text          | NO   |     | NULL    |       |
| quantityInStock    | smallint(6)   | NO   |     | NULL    |       |
| buyPrice           | decimal(10,2) | NO   |     | NULL    |       |
| MSRP               | decimal(10,2) | NO   |     | NULL    |       |
+--------------------+---------------+------+-----+---------+-------+
9 rows in set (0.010 sec)

DESCRIBE productlines;
MariaDB [classic_models]> DESCRIBE productlines;
+-----------------+---------------+------+-----+---------+-------+
| Field           | Type          | Null | Key | Default | Extra |
+-----------------+---------------+------+-----+---------+-------+
| productLine     | varchar(50)   | NO   | PRI | NULL    |       |
| textDescription | varchar(4000) | YES  |     | NULL    |       |
| htmlDescription | mediumtext    | YES  |     | NULL    |       |
| image           | mediumblob    | YES  |     | NULL    |       |
+-----------------+---------------+------+-----+---------+-------+
4 rows in set (0.014 sec)

 */
-- 18. Show the customer's name with a phone number containing "+81".

SELECT customerName FROM customers WHERE phone LIKE "%+81%";

/* 
DESCRIBE customers;
 */

-- Find all customers from US

SELECT * FROM customers WHERE country = "USA";

-- Show the full details of a customer named La Rochelle Gifts.

SELECT * FROM customers WHERE customerName = "La Rochelle Gifts";

=======================================
	MYSQL- STRETCH GOALS
=======================================

-- 1. Return the customer names of customers whose customer names don't have 'a' in them
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

-- 2. Return the last names and first names of employees being supervised by "Anthony Bow"
SELECT lastName, firstName FROM employees WHERE reportsTo = 1056;

-- 3. Return the product name and MSRP of the product with the highest MSRP

SELECT productName, MSRP FROM products ORDER BY MSRP DESC LIMIT 1;

-- 4. Return the number of customers in the UK
SELECT COUNT(*) FROM customers WHERE country = "UK";

-- 5. Return the number of products per product line



SELECT COUNT(productName), products.productline  FROM products
       	JOIN productlines ON products.productLine = productlines.productLine
		GROUP BY products.productline;

-- 6. Return the number of customers served by every employee
SELECT COUNT(customers.customerName), firstName, lastName  FROM employees
       	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
		GROUP BY customers.salesRepEmployeeNumber ORDER BY firstName ASC;


-- 7. Show the customer's name with the highest credit limit.


SELECT customerName, max(creditLimit) FROM customers;